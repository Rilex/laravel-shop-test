<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Home page controler
     * 
     * @param Request $request
     */

    public function getIndex()
    {
        $products = Product::orderBy('id', 'desc')
            ->take(6)
            ->get();

        $products_popular = Product::orderBy('id', 'desc')
            ->where('stars', 4)
            ->take(5)
            ->get();

        return view('page.index', [
            'products' => $products,
            'products_popular' => $products_popular,
        ]);
    }

    /**
     * Products page controler
     * 
     * @param Request $request
     */
    public function getProducts(Request $request)
    {
        $brands = Brand::withCount(['products'])
            ->orderBy('products_count', 'desc')
            ->get();

        $brand_active = Brand::find($request->brand);

        $products = Product::order($request->sortby)
            ->filterbrand($request->brand)
            ->priceMin($request->price_min)
            ->priceMax($request->price_max)
            ->paginate(12);

        return view('page.products', [
            'brands' => $brands,
            'brand_active' => $brand_active,
            'products' => $products,
            'request' => $request
        ]);
    }

    /**
     * Checkout controler
     * We are only making checkout for single item on this test
     * 
     * @param Request $request
     */
    public function getCheckout(Request $request)
    {
        $item = Product::find($request->product);

        return view('page.checkout', [
            'item' => $item,
        ]);
    }

    /**
     * Procces Payment
     * 
     * @param Request $request
     */
    public function postPayment(Request $request)
    {
        $product = Product::find($request->item);
        return Order::charge($request, $product);
    }
}
