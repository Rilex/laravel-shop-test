<?php

namespace App\Mail;

use App\Order;
use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Demo $Order
     */
    public $order;

    /**
     * @var Product $Product
     */
    public $product;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order, Product $product)
    {
        $this->order = $order;
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('someone@test.com')
            ->text('mail.order_plain')
            ->subject('Customer ' . $this->order->client_name . ' purchased 1 item from you as part of Order # ' . $this->order->id);
    }
}
