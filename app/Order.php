<?php

namespace App;

use App\Mail\OrderEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use \Stripe\Stripe;

class Order extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'total_product_value',
        'total_shipping_value',
        'client_name',
        'client_address'
    ];

    /**
     * Charge order via stripe
     * 
     * @param object $form
     * @param Product $product
     */
    public static function charge($form, Product $product)
    {
        $fields = json_decode($form->customer[0]);
        Stripe::setApiKey(env('STRIPE_SECRET'));

        /* Check if customer form is complete  */
        foreach ($fields as $key => $value) {
            $value = trim($value);
            if ($key == 'shipping') continue;
            if (empty($value))
                return response()->view('page.checkout', [
                    'item' => Product::find($product->id),
                    'error' => ucfirst($key) . ' Cannot be empty.'
                ])->setStatusCode(406);;
        }

        // Prevent tempering with form value
        if ($fields->shipping != 0) $fields->shipping = 10;

        try {
            $charge =  \Stripe\Charge::create([
                'amount' => ($product->price + $fields->shipping) * 100,
                'currency' => 'eur',
                'source' => $form->stripeToken,
                'description' => 'Charge for ' . $product->name,
            ]);
        } catch (\Throwable $th) {
            return response()->view('page.checkout', [
                'item' => Product::find($product->id),
                'error' => 'Payment Error, please try again...'
            ])->setStatusCode(400);;
        }

        /* Fill Order */
        $order = new Order();
        $order->fill(array(
            'total_product_value' => $product->price,
            'total_shipping_value' => $product->price + $fields->shipping,
            'client_name' => $fields->name,
            'client_address' => $fields->address
        ));
        $order->save();

        Mail::to('temom46780@tmailpro.net')
            ->send(new OrderEmail($order, $product));

        return view('page.order_placed');
    }
}
