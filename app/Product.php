<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'name',
        'price',
        'price_old',
        'image',
        'stars'
    ];

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo 
     */
    public function brand() {
        return $this->belongsTo('\App\Brand');
    }

    /**
     * Set query order
     * 
     * @param $query
     * @param string|null $order
     */
    public function  scopeOrder($query, $order): object
    {
        switch ($order) {
            case 'Relevance':
                return $query->orderBy('id', 'ASC');
            case 'Name, A to Z':
                return $query->orderBy('name', 'ASC');
            case 'Name, Z to A':
                return $query->orderBy('name', 'DESC');
            case 'Price, low to high':
                return $query->orderBy('price', 'ASC');
            case 'Price, high to low':
                return $query->orderBy('price', 'DESC');
            default:
                return $query;
        }
    }

    /**
     * Filter minimal price
     * 
     * @param $query
     * @param int|null $price_min
     */
    public function  scopePriceMin($query, $price_min): object
    {
        if ($price_min)
            return $query->where('price', '>=', $price_min);
        return $query;
    }

    /**
     * Filter max price
     * 
     * @param $query
     * @param int|null $price_max
     */
    public function  scopePriceMax($query, $price_max): object
    {
        if ($price_max)
            return $query->where('price', '<=', $price_max);
        return $query;
    }

    /**
     * Filter by brand
     * 
     * @param $query
     * @param int|null $brand
     */
    public function scopeFilterBrand($query, $brand)
    {
        if ($brand)
            return $query->where('brand_id', $brand);
        return $query;
    }

}
