<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $price  = $faker->randomFloat(2, 50, 500);
    $images = [
        '1.webp',
        '2.webp',
        '3.webp',
        '4.webp',
        '5.webp',
        '6.webp',
        '7.webp',
        '8.webp',
        '9.webp',
        '10.webp',
        '11.webp',
        '12.webp',
        '13.webp',
        '14.webp',
    ];

    return [
        'brand_id' => function () {
            return factory(Product::class)->create()->id;
        },
        'name' => $faker->words(rand(2, 4), true),
        'price' => ceil($price),
        'price_old' => (rand(0, 10) > 3) ?  NULL : ceil(1.3 * $price),
        'image' => $images[array_rand($images)],
        'stars' => (rand(0, 10) > 2) ? NULL : rand(4, 5)
    ];
});
