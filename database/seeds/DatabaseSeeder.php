<?php

use App\Brand;
use App\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create brands
        factory(Brand::class, 7)->create()->each(function ($brand) {

            // create random products for each brand
            factory(Product::class, rand(1,1500))->create(['brand_id' => $brand->id]);
        });

    }
}