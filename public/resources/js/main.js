/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

AOS.init({
  duration: 800,
  easing: 'slide',
  once: true
});
jQuery(document).ready(function ($) {
  "use strict";

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  var searchShow = function searchShow() {
    // alert();
    var searchWrap = $('.search-wrap');
    $('.js-search-open').on('click', function (e) {
      e.preventDefault();
      searchWrap.addClass('active');
      setTimeout(function () {
        searchWrap.find('.form-control').focus();
      }, 300);
    });
    $('.js-search-close').on('click', function (e) {
      e.preventDefault();
      searchWrap.removeClass('active');
    });
  };

  searchShow();
  /**
   * Price slider
   * 
   */

  $("#slider-range").on("slidestop", function () {
    filterFormSubmit(true);
  });
  /**
   * Re-Input changed values to filter form
   * 
   * @param bool submit
   */

  var filterFormSubmit = function filterFormSubmit(submit) {
    $("#product_filter input[name='price_min']").val($("#slider-range").slider("values")[0]);
    $("#product_filter input[name='price_max']").val($("#slider-range").slider("values")[1]);
    $("#product_filter input[name='page']").val(1);
    if (submit) $("#product_filter").submit();
  };
  /**
   * Get url parameter
   * 
   * @param string name 
   */


  var getUrlParameter = function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.href);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, '    '));
  };

  var slider = function slider() {
    $('.nonloop-block-3').owlCarousel({
      center: false,
      items: 1,
      loop: false,
      stagePadding: 15,
      margin: 20,
      nav: true,
      navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
      responsive: {
        600: {
          margin: 20,
          items: 2
        },
        1000: {
          margin: 20,
          items: 3
        },
        1200: {
          margin: 20,
          items: 3
        }
      }
    });
  };

  slider();

  var siteMenuClone = function siteMenuClone() {
    $('<div class="site-mobile-menu"></div>').prependTo('.site-wrap');
    $('<div class="site-mobile-menu-header"></div>').prependTo('.site-mobile-menu');
    $('<div class="site-mobile-menu-close "></div>').prependTo('.site-mobile-menu-header');
    $('<div class="site-mobile-menu-logo"></div>').prependTo('.site-mobile-menu-header');
    $('<div class="site-mobile-menu-body"></div>').appendTo('.site-mobile-menu');
    $('.js-logo-clone').clone().appendTo('.site-mobile-menu-logo');
    $('<span class="ion-ios-close js-menu-toggle"></div>').prependTo('.site-mobile-menu-close');
    $('.js-clone-nav').each(function () {
      var $this = $(this);
      $this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
    });
    setTimeout(function () {
      var counter = 0;
      $('.site-mobile-menu .has-children').each(function () {
        var $this = $(this);
        $this.prepend('<span class="arrow-collapse collapsed">');
        $this.find('.arrow-collapse').attr({
          'data-toggle': 'collapse',
          'data-target': '#collapseItem' + counter
        });
        $this.find('> ul').attr({
          'class': 'collapse',
          'id': 'collapseItem' + counter
        });
        counter++;
      });
    }, 1000);
    $('body').on('click', '.arrow-collapse', function (e) {
      var $this = $(this);

      if ($this.closest('li').find('.collapse').hasClass('show')) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }

      e.preventDefault();
    });
    $(window).resize(function () {
      var $this = $(this),
          w = $this.width();

      if (w > 768) {
        if ($('body').hasClass('offcanvas-menu')) {
          $('body').removeClass('offcanvas-menu');
        }
      }
    });
    $('body').on('click', '.js-menu-toggle', function (e) {
      var $this = $(this);
      e.preventDefault();

      if ($('body').hasClass('offcanvas-menu')) {
        $('body').removeClass('offcanvas-menu');
        $this.removeClass('active');
      } else {
        $('body').addClass('offcanvas-menu');
        $this.addClass('active');
      }
    }); // click outisde offcanvas

    $(document).mouseup(function (e) {
      var container = $(".site-mobile-menu");

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('offcanvas-menu')) {
          $('body').removeClass('offcanvas-menu');
        }
      }
    });
  };

  siteMenuClone();

  var sitePlusMinus = function sitePlusMinus() {
    $('.js-btn-minus').on('click', function (e) {
      e.preventDefault();

      if ($(this).closest('.input-group').find('.form-control').val() != 0) {
        $(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
      } else {
        $(this).closest('.input-group').find('.form-control').val(parseInt(0));
      }
    });
    $('.js-btn-plus').on('click', function (e) {
      e.preventDefault();
      $(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
    });
  };

  sitePlusMinus();

  var siteSliderRange = function siteSliderRange() {
    $("#slider-range").slider({
      range: true,
      min: 0,
      max: 600,
      values: [getUrlParameter('price_min') ? getUrlParameter('price_min') : 50, getUrlParameter('price_max') ? getUrlParameter('price_max') : 500],
      slide: function slide(event, ui) {
        $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
      }
    });
    $("#amount").val("€" + $("#slider-range").slider("values", 0) + " - €" + $("#slider-range").slider("values", 1));
  };

  siteSliderRange();

  var siteMagnificPopup = function siteMagnificPopup() {
    $('.image-popup').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      closeBtnInside: false,
      fixedContentPos: true,
      mainClass: 'mfp-no-margins mfp-with-zoom',
      // class to remove default margin from left and right side
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0, 1] // Will preload 0 - before current, and 1 after the current image

      },
      image: {
        verticalFit: true
      },
      zoom: {
        enabled: true,
        duration: 300 // don't foget to change the duration also in CSS

      }
    });
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    });
  };

  siteMagnificPopup();
  /**
   * Select boxes on product listing
   */

  $("#filter-select a").click(function (e) {
    e.preventDefault();
    var parent_id = $(this).parent().parent().find('button').attr('id');
    var value = $(this).attr('href');
    if (parent_id === "dropdownMenuOffset") $("#product_filter input[name='brand']").val(value);else $("#product_filter input[name='sortby']").val(value);
    $("#product_filter input[name='page']").val(1);
    $("#product_filter").submit();
  });
});

/***/ }),

/***/ "./resources/sass/style.scss":
/*!***********************************!*\
  !*** ./resources/sass/style.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!****************************************************************!*\
  !*** multi ./resources/js/main.js ./resources/sass/style.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! v:\rilex\docker\uf-node-apache\uf-node-apache-www\www\urbanforge.cf\resources\js\main.js */"./resources/js/main.js");
module.exports = __webpack_require__(/*! v:\rilex\docker\uf-node-apache\uf-node-apache-www\www\urbanforge.cf\resources\sass\style.scss */"./resources/sass/style.scss");


/***/ })

/******/ });
//# sourceMappingURL=main.js.map