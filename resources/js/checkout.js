jQuery(document).ready(function ($) {

    "use strict";

	/**
	 * Toggle shipping select
	 */
    $("#c_shipping").click(function (e) {
        if ($(this).prop('checked')) {
            $("#t_price").text("$" + (parseFloat($("#payment-form input[name='price']").val()) + 10).toFixed(2));
            $("#shipping-display").text('10 EUR');
            $(this).prop('checked', true);
        }
        else {
            $("#t_price").text("$" + (parseFloat($("#payment-form input[name='price']").val())).toFixed(2));
            $("#shipping-display").text('Free');
            $(this).prop('checked', false);
        }
    });

    /* STRIPE START */
    var stripe = Stripe($("#payment-form input[name='stripe_key']").val());
    // Create an instance of Elements.
    var elements = stripe.elements();
    var style = {
        base: {
            border: '1px solid rgb(220, 208, 208)',
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };
    // Create an instance of the card Element.
    var card = elements.create('card', { hidePostalCode: true, style: style });
    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');
    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function (event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });
    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();

        stripe.createToken(card).then(function (result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });
    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        $("input[name='customer[]']").val(JSON.stringify({
            name: $("#cname").val() + " " + $("#clname").val(),
            address: $("#caddress").val() + " " + $("#c_state").val() + " " + $("#c_zip").val(),
            shipping: $("#c_shipping").is(':checked') ? $("#c_shipping").val() : 0
        }));
        // Submit the form
        form.submit();
    }
    /* STRIPE END */

}); 