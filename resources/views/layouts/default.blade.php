<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>@yield('title')</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700">
    <link rel="stylesheet" href="{{ asset('resources/fonts/icomoon/style.css') }}"> 

    <link href="{{ asset('resources/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('resources/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('resources/css/aos.css') }}">
    <link rel="stylesheet" href="{{ mix('resources/css/style.css') }}"> 
    @yield('styles')

</head>

<body>

    <div class="site-wrap">
        @include('partials.navbar')
        @yield('content')
        @include('partials.footer')
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{ asset('resources/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('resources/js/popper.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="{{ asset('resources/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('resources/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('resources/js/aos.js') }}"></script>
    <script src="{{ mix('resources/js/main.js') }}"></script>
    @yield('scripts')
</body>

</html>