@extends('layouts.default')

@section('title',config('app.name') . ' - Checkout')

@section('content')

@include('partials.breadcrumb')

<div class="site-section">
    <div class="container">

        @if(isset($error))
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endif

        <div class="row">

            <div class="col-md-6 mb-5 mb-md-0">

                <h2 class="h3 mb-3 text-black">Billing Details</h2>

                <div class="p-3 p-lg-5 border">
                    <form id="customer">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="c_fname" class="text-black">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="cname" name="name">
                            </div>
                            <div class="col-md-6">
                                <label for="c_lname" class="text-black">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="clname" name="lname">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="c_address" class="text-black">Address <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="caddress" name="address" placeholder="Street address">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="c_state_country" class="text-black">State / Country <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="c_state" name="country">
                            </div>
                            <div class="col-md-6">
                                <label for="c_postal_zip" class="text-black">Postal / Zip <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="c_zip" name="zip">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="c_create_account" class="text-black">
                                <input type="checkbox" value="10" id="c_shipping" name="shipping"> Express shipping (10 EUR)
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">



                <div class="row mb-5">
                    <div class="col-md-12">
                        <h2 class="h3 mb-3 text-black">Your Order</h2>
                        <div class="p-3 p-lg-5 border">
                            <table class="table site-block-order-table mb-5">
                                <thead>
                                    <th>Product</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $item->name }}<strong class="mx-2">x</strong> 1</td>
                                        <td>{{ $item->price }} EUR</td>
                                    </tr>
                                    <tr>
                                        <td class="text-black font-weight-bold"><strong>Cart Subtotal</strong></td>
                                        <td class="text-black">{{ $item->price }} EUR</td>
                                    </tr>

                                    <tr>
                                        <td class="text-black">Shipping</td>
                                        <td class="text-black" id="shipping-display">Free</td>
                                    </tr>

                                    <tr>
                                        <td class="text-black font-weight-bold"><strong>Order Total</strong></td>
                                        <td class="text-black font-weight-bold"><strong id="t_price">{{ $item->price }} EUR</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="form-group">

                                <form action="payment" method="post" id="payment-form">

                                    <label for="card-element">
                                        Credit or debit card
                                    </label>
                                    <div id="card-element">
                                    </div>

                                    <div id="card-errors" role="alert"></div>
                                    <style>
                                        .StripeElement {
                                            box-sizing: border-box;
                                            height: 40px;
                                            padding: 10px 12px;
                                            border: 1px solid rgb(220, 208, 208);
                                            border-radius: 4px;
                                            background-color: white;
                                            box-shadow: 0 1px 3px 0 #e6ebf1;
                                            -webkit-transition: box-shadow 150ms ease;
                                            transition: box-shadow 150ms ease;
                                        }

                                        .StripeElement--focus {
                                            box-shadow: 0 1px 3px 0 #cfd7df;
                                        }

                                        .StripeElement--invalid {
                                            border-color: #fa755a;
                                        }

                                        .StripeElement--webkit-autofill {
                                            background-color: #fefde5 !important;
                                        }
                                    </style>
                                    @csrf
                                    <input type="hidden" name="customer[]">
                                    <input type="hidden" name="item" value="{{ $item->id }}">
                                    <input type="hidden" name="price" value="{{ $item->price }}">
                                    <input type="hidden" name="stripe_key" value="{{ env('STRIPE_API_KEY') }}">
                                    <button class="btn btn-primary btn-lg btn-block mt-4">PLACE ORDER</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- </form> -->
    </div>
</div>

@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script src="{{ mix('resources/js/checkout.js') }}"></script>
@endsection