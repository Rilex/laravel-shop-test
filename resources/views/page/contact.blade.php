@extends('layouts.default')

@section('title',config('app.name') . ' - Contact Us')

@section('content')

@include('partials.breadcrumb')

<div class="site-section">
    <div class="container">
        <div class="row">
            <h2 class="h3 mb-3 text-black">Get In Touch</h2>
        </div>
    </div>
</div>
@endsection