@extends('layouts.default')

@section('title',config('app.name'))

@section('content')
@include('partials.hero1')

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="title-section mb-5 col-12">
                <h2 class="text-uppercase">New Products</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($products as $item)
            <div class="col-lg-4 col-md-6 item-entry mb-4">
                <a href="{{ URL::to('checkout') . '?product=' . $item->id }}" class="product-item md-height bg-gray d-block">
                    <img src="{{ asset('resources/images/'.$item->image) }}" alt="Image" class="img-fluid">
                    <p>BUY NOW</p>
                </a>
                <h2 class="item-title"><a href="#">{{ ucwords($item->name) }}</a></h2>
                <strong class="item-price"><del>{{ $item->price_old ? '$' . $item->price_old : '' }}</del> ${{ $item->price }}</strong>
                <div class="star-rating">
                    @for ($i = 1; $i <= $item->stars; $i++)
                    <span class="icon-star2 text-warning"></span>
                    @endfor
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="title-section text-center mb-5 col-12">
                <h2 class="text-uppercase">Most Rated</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 block-3">
                <div class="nonloop-block-3 owl-carousel">
                    @foreach ($products_popular as $item)
                    <div class="item">
                        <div class="item-entry">
                            <a href="{{ URL::to('checkout') . '?product=' . $item->id }}" class="product-item md-height bg-gray d-block">
                                <img src="{{ asset('resources/images/'.$item->image) }}" alt="Image" class="img-fluid">
                                <p>BUY NOW</p>
                            </a>
                            <h2 class="item-title"><a href="#">{{ ucwords($item->name) }}</a></h2>
                            <strong class="item-price"><del>{{ $item->price_old ? '$' . $item->price_old : '' }}</del> ${{ $item->price }}</strong>
                            <div class="star-rating">
                                @for ($i = 1; $i <= $item->stars; $i++)
                                <span class="icon-star2 text-warning"></span>
                                @endfor
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@include('partials.hero2')
@endsection