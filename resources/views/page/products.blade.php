@extends('layouts.default')

@section('title',config('app.name') . ' - Products')

@section('content')

@include('partials.breadcrumb')

<div class="site-section">
    <div class="container">

        <div class="row mb-5">
            <div id="products-grid" class="col-md-9 order-1">

                @include('partials.productsgrid')

            </div>

            <div class="col-md-3 order-2 mb-5 mb-md-0">
                <div class="border p-4 rounded mb-4">
                    <h3 class="mb-3 h6 text-uppercase text-black d-block">Brands</h3>
                    <ul class="list-unstyled mb-0">
                        @foreach ($brands as $item)
                        <li class="mb-1"><a href="{{ URL::to('brands?brand='. $item->id) }}" class="d-flex"><span>{{ $item->name }}</span> <span class="text-black ml-auto">({{ number_format($item->products_count) }})</span></a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="border p-4 rounded mb-4">
                    <div class="mb-4">
                        <h3 class="mb-3 h6 text-uppercase text-black d-block">Filter by Price</h3>
                        <div id="slider-range" class="border-primary"></div>
                        <input type="text" value="" name="text" id="amount" class="form-control border-0 pl-0 bg-white" disabled="" />
                    </div>

                </div>
            </div>

            <form  id="product_filter" action="" method="GET">
                <input type="hidden" name="brand" value="{{ $brand_active->id ?? '' }}">
                <input type="hidden" name="sortby" value="{{ request('sortby') }}">
                <input type="hidden" name="price_min" value="{{ request('price_min') }}">
                <input type="hidden" name="price_max" value="{{ request('price_max') }}">
                <input type="hidden" name="page" value="{{ request('page') }}">
            </form>
            
        </div>

    </div>
</div>

@endsection