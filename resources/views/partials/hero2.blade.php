<div class="site-blocks-cover inner-page py-5" data-aos="fade">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto order-md-2 align-self-start">
                <div class="site-block-cover-content">
                    <h2 class="sub-title">#New Summer Collection 2020</h2>
                    <h1>New Arrivals</h1>
                    <p><a href="{{ URL::to('/products') }}" class="btn btn-black rounded-0">Browse Brands</a></p>
                </div>
            </div>
            <div class="col-md-6 order-1 align-self-end">
                <img src="{{ asset('resources/images/hero2.jpg') }}" alt="Image" class="img-fluid">
            </div>
        </div>
    </div>
</div>