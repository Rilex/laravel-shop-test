<div class="row align">
    <div class="col-md-12 mb-5">

        <div class="d-flex" id="filter-select">
            <div class="dropdown mr-1 ml-md-auto">

                <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ $brand_active->name ??  'All Brands' }}
                </button>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                    @foreach ($brands as $item)
                    <a class="dropdown-item" href="{{ $item->id }}">{{ $item->name }}</a>
                    @endforeach
                </div>

            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuReference" data-toggle="dropdown">
                    {{ request('sortby') ?? 'Reference' }}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                    <a class="dropdown-item" href="Relevance">Relevance</a>
                    <a class="dropdown-item" href="Name, A to Z">Name, A to Z</a>
                    <a class="dropdown-item" href="Name, Z to A">Name, Z to A</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="Price, low to high">Price, low to high</a>
                    <a class="dropdown-item" href="Price, high to low">Price, high to low</a>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row mb-5">

    @foreach ($products as $item)
    <div class="col-lg-6 col-md-6 item-entry mb-4">
        <a href="{{ URL::to('checkout') . '?product=' . $item->id }}" class="product-item md-height bg-gray d-block">
            <img src="{{ asset('resources/images/'.$item->image) }}" alt="Image" class="img-fluid">
            <p>BUY NOW</p>
        </a>
        <h2 class="item-title"><a href="{{ URL::to('checkout') . '?product=' . $item->id }}">{{ ucwords($item->name) }}</a></h2>
        <strong class="item-price"><del>{{ $item->price_old ? '€' . $item->price_old : '' }}</del> €{{ $item->price }}</strong>
        <div class="star-rating">
            @for ($i = 1; $i <= $item->stars; $i++)
                <span class="icon-star2 text-warning"></span>
                @endfor
        </div>
    </div>
    @endforeach
</div>

{{ $products->appends(request()->query())->links() }}