@if ($paginator->hasPages())
<div class="row">
    <div class="col-md-12 text-center">
        <div class="site-block-27">
            <ul>
                @if (!$paginator->onFirstPage())
                    <li><a href="{{ $paginator->previousPageUrl() }}">&lt;</a></li>
                @endif
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- "Three Dots" Separator --}}
                    @if (is_string($element))
                        <li class="disabled"><span>...</span></li>
                    @endif
                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active"><a>{{ $page }}</a></li>
                            @else
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}">&gt;</a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
@endif


