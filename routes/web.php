<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'PageController@getIndex',
    'as' => 'Home'
]);

Route::get('products', [
    'uses' => 'PageController@getProducts',
    'as' => 'Products'
]);

Route::get('brands', [
    'uses' => 'PageController@getProducts',
    'as' => 'Brands'
]);

Route::get('contact', function () {
    return view('page.contact');
})->name('Contact');

Route::get('checkout', [
    'uses' => 'PageController@getCheckout',
    'as' => 'Checkout'
]);

Route::post('payment', [
    'uses' => 'PageController@postPayment',
    'as' => 'Payment'
]);