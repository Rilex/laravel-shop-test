<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use RefreshDatabase;

    /* @test */
    public function testHomepageStatus()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    /* @test */
    public function noGetOnCheckout()
    {
        $response = $this->get('payment');
        $response->assertStatus(405);
    }
}
