const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/main.js', 'public/resources/js')
    .js('resources/js/checkout.js', 'public/resources/js')
    .sass('resources/sass/style.scss', 'public/resources/css')
    .sourceMaps()
    .disableNotifications()
    .webpackConfig({
        devtool: 'source-map'
    })
    .options({
        processCssUrls: false
    });